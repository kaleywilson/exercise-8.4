package paystation.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestCurrencies {
	PayStation ps;

	@Before
	public void setUp() {
		ps = new PayStationImpl(new LinearRateStrategy(), new DanishCurrency());
	}

	/** Test acceptance of all legal coins */
	@Test
	public void shouldAcceptLegalCoins() throws IllegalCoinException {
		ps.addPayment(1);
		ps.addPayment(2);
		ps.addPayment(5);
		ps.addPayment(10);
		ps.addPayment(20);
		assertEquals("Should accept 1, 2, 5, 10, and 20 kroners", (5 + 10 + 1 + 2 + 20)*7, ps.readDisplay());
	}

	/**
	 * Verify that illegal coin values are rejected.
	 */
	@Test(expected = IllegalCoinException.class)
	public void shouldRejectIllegalCoin() throws IllegalCoinException {
		ps.addPayment(17);
	}
	
	@Test
	public void testProgressiveRateStrategy() {
		RateStrategy rs = new ProgressiveRateStrategy();

		int amount1 = 25;
		int amount2 = 15;
		assertEquals(120+5*5, rs.calculateTime(amount1));
		assertEquals(15*6, rs.calculateTime(amount2));
		
	}
}
