package paystation.domain;

public interface CurrencyStrategy {
	/**
	 * This method checks to see if the coin value
	 * is legal or not. Throws IllegalCoinException if not
	 * legal
	 * @param coin value
	 * @throws IllegalCoinException 
	 */
	public void checkCoinValue(int coinValue) throws IllegalCoinException;
}
