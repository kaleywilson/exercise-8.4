package paystation.domain;

public class UnitedStatesCurrency implements CurrencyStrategy{
	public UnitedStatesCurrency() {
		
	}
	
	@Override
	public void checkCoinValue(int coinValue)
		throws IllegalCoinException {
		    switch ( coinValue ) {
		    case 5: break;
		    case 10: break;  
		    case 25: break;  
		    default: 
		      throw new IllegalCoinException("Invalid coin: "+coinValue);
		    }
	}

}
